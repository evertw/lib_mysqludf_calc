/** @file cal_bestratio.cpp
 *  @brief Implementation of the calc_bestratio MySQL UDF
 * 
 *  @author Evert Wipplinger
 *  @version 0.0.3
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lib_mysqludf_calc.h"
#include "bestratio.h"

/** Virtual deinitialization calc_best_ratio UDF */
void calc_bestratio_deinit( UDF_INIT *initid ) {};

/** Initialization of calc_best_ratio UDF */
my_bool calc_bestratio_init(  UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	initid->maybe_null=0;

	bool isconst = 1;
	for(size_t i=0; i<args->arg_count;i++) {
		if( args->args[i]==NULL) {			isconst=0;		}
	}
	if( isconst ) {		initid->const_item=1;		}
	isconst = 1;
	for(size_t i=0; i<args->arg_count;i++) {
		if( args->maybe_null[i]==1) {			isconst=0;		}
	}

	if( isconst==1 ) {		initid->const_item=1;	}

	switch ( args->arg_count ) {
		case 3:
			args->arg_type[2] = REAL_RESULT;
		case 2:
			args->arg_type[1] = INT_RESULT;
			args->arg_type[0] = REAL_RESULT;
		break;
		default :
			strcpy(message,"calc_bestratio_init(target_ratio,max_denom,error) requires 2-3 arguments");
			return 1;
	}
	initid->decimals=17;
	return 0;
}

/** calculates the best approximation
 * @note: The approximation may be infeasible and calc_bestratio returns NULL in this case
 */
double calc_bestratio( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error ) {
	for(size_t i=0;i<args->arg_count;i++) {
		if( args->args[i] == NULL) {			*is_null=1; 			return 0.0;		}
	}
	longlong max_denom = 17;
	const double target_ratio = *(double *)(args->args[0]);
	long double ipart;
	const long double fpart = modf(target_ratio,&ipart);

	switch (args->arg_count) {
		case 3:  {
			max_denom = *((longlong *)(args->args[1]));
			const double max_err = *((double *)(args->args[2]));
			const ratx_ratio r = ratx_find_best_rat_with_err_bound_cont(   fpart ,  max_denom	, max_err);
			if( ratx_check_ratio(&r,fpart ,  max_denom	, max_err) )  {
					*is_null=1;
					return 0;
			}
			return ratx_get_ratio(&r) + ipart;
		} break;
		case 2: {
			max_denom = *(longlong *)(args->args[1]);
		case 1:
			const ratx_ratio r = ratx_find_best_rat_cont(   fpart ,  max_denom);
			if( ratx_check_ratio(&r,fpart ,  max_denom	, 0.0) )  {
					*is_null=1;
					return 0;
			}
			return ratx_get_ratio(&r) + ipart;
		} break;
		default:
			*error=1;
			*is_null=1;
			return 0;
	} //end switch
	*error=1;
	*is_null=1;
	return 0;
}


