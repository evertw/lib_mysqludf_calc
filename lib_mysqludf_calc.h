/** @file  lib_mysqludf_calc.h
 *  @brief lib_mysqludf_calc - a library adding additional calculation functions to MySQL.
 *  @author Evert Wipplinger
*/

/** @mainpage  lib_mysqludf_calc
 *  @brief lib_mysqludf_calc - a library adding additional calculation functions to MySQL.
 *  @version 0.0.3
 *  @author Evert Wipplinger
 *  @copyright (C) 2015 Evert Wipplinger. Released under GNU General Public License version 3.
 *  @since 2011
 *
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/


/** @def DEBUG_PRINTF
 *  fprintf for debugging purposes
 */
#ifdef DEBUG
	#define DEBUG_PRINTF( fmt , ... ) fprintf(stdout, fmt , __VA_ARGS )
#else
	#define DEBUG_PRINTF(... ) ((void)0)
#endif


//#define DLLEXP __declspec(dllexport) 
/** @def DLLEXP 
 * defintion of windows dll exporter 
 * @note The definition is empty because the exports are defined in lib_mysqludf_calc.def
 * @verbinclude lib_mysqludf_calc.def
 */

#if defined(_WIN32) || defined(_WIN64) || defined(__WIN32__) || defined(WIN32)
#define DLLEXP 
#else
#define DLLEXP
#endif

#ifdef STANDARD
#include <string.h>
#include <stdlib.h>
#include <time.h>
#ifdef __WIN__
typedef unsigned __int64 ulonglong;
typedef __int64 longlong;
#else
typedef unsigned long long ulonglong;
typedef long long longlong;
#endif /*__WIN__*/
#else
#include <my_global.h>
#include <my_sys.h>
#endif
#include <mysql.h>
#include <m_ctype.h>
#include <m_string.h>
#include <stdlib.h>

#include <ctype.h>

//#ifdef HAVE_DLOPEN
#ifdef	__cplusplus
extern "C" {
#endif

/** Library version for use in #lib_mysqludf_calc_info*/
#define LIBVERSION "lib_mysqludf_calc version 0.0.3"

/** @defgroup Mysql_UDF_lib_mysqludf_calc_info lib_mysqludf_calc_info 
 *  @brief Library Information 
 *  @return Library version string #LIBVERSION
 *  @{
 */
DLLEXP   char* lib_mysqludf_calc_info(	UDF_INIT *initid,	UDF_ARGS *args,	char* result,	unsigned long* length,	char *is_null,	char *error	);	   	/**< returns library info*/
DLLEXP my_bool lib_mysqludf_calc_info_init(	UDF_INIT *initid,	UDF_ARGS *args,	char *message);															/**< udf init*/
DLLEXP    void lib_mysqludf_calc_info_deinit(	UDF_INIT *initid);																					   	/**< udf deinit*/
/** @} */ // end of Mysql_UDF_lib_mysqludf_calc_info<cmp>

/** @defgroup Mysql_UDF_ltr_incr_ ltr_inc_<cmp>
 *  Incremental compare of paris from left to right until a condition is satisfied
 *  
 *  
 *  These convenience functions compare pairs of arguments until a condition is satisfied.
 *  Therefore the number of arguments must be an even number.
 *  They can be used to replace CASE WHEN ... THEN ... statetments.
 *  
 *  @see #ltr_incr_gt  
 *  Compares each left, right pair until left > right
 *  @see ltr_incr_lt 
 *  Compares each left, right pair until left < right
 *  @see ltr_incr_lt 
 *  Compares each left, right pair until left != right
 *
 *  @param left1 (REAL OR INT): left element of a first pair
 *  @param right1 (REAL OR INT): right element of first
 *  @param more_pairs (left2, right2, left3, right3, .... )
 *  @return The index of the first pair that satisfies a condition: so if the second pair satisfies the condition 2 is returned.
 *  @return NULL if a null value is encountered before the condition matches
 *  @return 0 if the condition never matches
 *
 *  @todo sepcialise #my_argcmp
 *  @see #Mysql_UDF_ltr_firstarg_
 *  @{
 */
DLLEXP longlong ltr_incr_gt(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);														   	/**< search left-to-right for the first pair where left > right*/
DLLEXP  my_bool ltr_incr_gt_init( UDF_INIT *initid, UDF_ARGS *args, char *message );																   	/**< ltr_incr_gt init*/
DLLEXP     void ltr_incr_gt_deinit( UDF_INIT *initid );																							   	/**< ltr_incr_gt deinit*/

DLLEXP longlong ltr_incr_lt(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);														   /**< search left-to-right for the first pair where left < right*/
DLLEXP  my_bool ltr_incr_lt_init( UDF_INIT *initid, UDF_ARGS *args, char *message );																   /**< ltr_incr_lt init*/
DLLEXP     void ltr_incr_lt_deinit( UDF_INIT *initid );																							   /**< ltr_incr_lt deinit*/

DLLEXP longlong ltr_incr_ne(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);														    /**< search left-to-right for the first pair where left != right*/
DLLEXP  my_bool ltr_incr_ne_init( UDF_INIT *initid, UDF_ARGS *args, char *message );																    /**< ltr_incr_lt init*/
DLLEXP     void ltr_incr_ne_deinit( UDF_INIT *initid );																							    /**< ltr_incr_lt deinit*/																																   	/**< calc_dtoa deinit*/
/** @} //end of Mysql_UDF_ltr_incr_ */

/** @defgroup Mysql_UDF_ltr_firstarg_ ltr_firstarg_<cmp>
 *  Incremental compare of paris from left to right until a condition is satisfied
 *  
 *  These convenience functions compare the first argument arg[0] to the remaining arguments arg[i] and returns the index i of the
 *  argument which satisfiees a comparison condition. 
 *  
 *  @see #ltr_firstarg_gton  
 *  arg[0] > arg[i] or arg[i] is NULL
 *  @see #ltr_firstarg_lton 
 *  arg[0] < arg[i] or arg[i] is NULL
 *  @see #ltr_incr_lt 
 *  arg[0] < arg[i]
 *  @see #ltr_incr_gt 
 *  arg[0] > arg[i]
 *
 *  @param arg0 (REAL OR INT, required)
 *  @param arg1 (REAL OR INT, required): first argument used for comparison
 *  @param arg[i] (REAL OR INT, optional): further argumets process from left to right
 *  @return i for which the comparison is ture 
 *  @return NULL if arg0 is NULL
 *  @return i+1 if no argument satisfies the condition but all arguments are not null
 *
 *  @todo sepcialise #my_argcmp
 *  @see #Mysql_UDF_ltr_incr_
 *  @{
 */
DLLEXP longlong ltr_firstarg_gton(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);		   	/**< search left-to-right for the first argument where arg[0] > arg[i] or arg[i] is null*/ 
DLLEXP  my_bool ltr_firstarg_gton_init( UDF_INIT *initid, UDF_ARGS *args, char *message );					/**< ltr_firstarg_gton init*/
DLLEXP     void ltr_firstarg_gton_deinit( UDF_INIT *initid );												/**< ltr_firstarg_gton deinit*/

DLLEXP longlong ltr_firstarg_lton(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);		   	/**< search left-to-right for the first argument where arg[0] < arg[i] or arg[i] is null*/ 
DLLEXP  my_bool ltr_firstarg_lton_init( UDF_INIT *initid, UDF_ARGS *args, char *message );					/**< ltr_firstarg_lton init*/
DLLEXP     void ltr_firstarg_lton_deinit( UDF_INIT *initid );												/**< ltr_firstarg_lton deinit*/

DLLEXP longlong ltr_firstarg_ge(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);		   	/**< search left-to-right for the first argument where arg[0] < arg[i]*/ 
DLLEXP  my_bool ltr_firstarg_ge_init( UDF_INIT *initid, UDF_ARGS *args, char *message );					/**< ltr_firstarg_ge init*/
DLLEXP     void ltr_firstarg_ge_deinit( UDF_INIT *initid );												/**< ltr_firstarg_ge deinit*/

DLLEXP longlong ltr_firstarg_le(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);		   	/**< search left-to-right for the first argument where arg[0] < arg[i]*/ 
DLLEXP  my_bool ltr_firstarg_le_init( UDF_INIT *initid, UDF_ARGS *args, char *message );					/**< ltr_firstarg_le init*/
DLLEXP     void ltr_firstarg_le_deinit( UDF_INIT *initid );												/**< ltr_firstarg_le deinit*/
/** @} //end of ltr_firstarg_<cmp> */

/** @defgroup Mysql_UDF_calc_bestratio calc_bestratio
 *  @brief MySQL UDF Functions for calculating the best rational approximation with an optional maximal error
 *  
 *  This function calculates the best rational apprxoimation to a rational number in
 *  the sense that it has the smallest denominator for which the following holds:
 *  The decimal representation of the rational approximation is within an error bound.
 *
 *  @note returns null if there is no feasible approximation
 *  @note If the third paramer, \a max_error i snot supplied, then no error is enforced and calc_bestratio 
 *  will return the best approximation and is guaranteed to return a result upon a not-null input.
 *  @warning it is recommended to supply a minimal number of digits for rounding to tackle cases when
 *  the shortest decimal representation has less digits than expected.
 *  
 *  @param target_ratio (REAL, required):  number which should be approximated 
 *  @param max_denom (INT, required):  the maximal denominator for the rational approximation
 *  @param max_error (INT, optional):  specifies the maximal allowed deviation from \a target_ratio to the returned approximation
 *  @return ratx_ratio, a structure from the bestratio library where numerator = ratx_ratio.num, and the denominator = ratx_ratio.denom
 *
 *  @see #Mysql_UDF_calc_bestratio_for_round
 *  @{
 */
DLLEXP  double calc_bestratio(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);														   	/**< calculates the best rational approximation for a highest denominator within bounds*/
DLLEXP my_bool calc_bestratio_init( UDF_INIT *initid, UDF_ARGS *args, char *message );																   	/**< calc_bestratio init*/
DLLEXP    void calc_bestratio_deinit( UDF_INIT *initid );																							    /**< calc_bestratio deini*/
/** @} */ // end of Mysql_UDF_calc_bestratio */

/** @defgroup Mysql_UDF_calc_bestratio_for_round calc_bestratio_for_round
 *  @brief MySQL UDF Functions for calculating the best rational approximation where target and approximation round to the same number.
 *  
 *  This function calculates the best rational apprxoimation to a rational number in
 *  the sense that it has the smallest denominator for which the following holds:
 *  The decimal representation of the rational approximation can be recovered
 *  when rounding at a specified number of digits \a round_at_digits.
 *
 *  @note The denominator is increasted by powers of 10 so the returned denominator can be
 *  the next power of ten larger than the parameter \a max_denom supplied.
 *  @warning The returned ratio is only guaranteed to satisfy the requirements if \a round_at_digits
 *  is smaller or equal to \a max_denom. Otherwose see the note.
 *  @note If the third paramer, \a max_denom i snot supplied, the rounding will take place at the
 *  number of signifincat digits o the fractional part of the shortest decimal representation of
 *  \a target_ratio (without trailing zeros).
 *  @warning it is recommended to supply a minimal number of digits for rounding to tackle cases when
 *  the shortest decimal representation has less digits than expected.
 *  
 *  @param target_ratio (REAL, required):  number which should be approximated 
 *  @param round_at_digits (INT, optional):  the number of digits at which when rounded the result should be equal to the \a target_ratio (default =17)
 *  @param max_denom (INT, optional):  the number of digits at which rounding must be equal
 *  @return ratx_ratio, a structure from the bestratio library where numerator = ratx_ratio.num, and the denominator = ratx_ratio.denom
 *
 *  @see #Mysql_UDF_calc_bestratio
 * @{
 */
DLLEXP double calc_bestratio_for_round(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);	/**< calc_bestratio_for_round main function*/
DLLEXP my_bool calc_bestratio_for_round_init( UDF_INIT *initid, UDF_ARGS *args, char *message );			/**< calc_bestratio_for_round init function*/
DLLEXP    void calc_bestratio_for_round_deinit( UDF_INIT *initid );											/**< calc_bestratio_for_round deinit function*/
/** @} end of Mysql_UDF_calc_bestratio_for_round*/


/** @defgroup Mysql_UDF_calc_quadroot_ calc_quadroot_<1,2>
 *  @brief MySQL UDF functions for calculating quadratic roots of a polynominal
 *  
 *  The quadratic roots of polynomial a x^2 + b x + c
 *  are the solutions to the equation a x^2 + b x + c = 0
 *  which are 1/(2a) * (-b +/- sqrt( b^2 - 4 ac) ) 
 *
 *  @see #calc_quadroot1 for the first quadratic root, the one further away from zero n distance
 *  @see #calc_quadroot2 for the second quadratic root, the one closer to zero in distance
 *  
 *  @param a (REAL, required):  quadratic coefficient
 *  @param b (REAL, required):  linear coefiicient
 *  @param c (REAL, required):  constant 
 *  @return calc_quadroot1: 1/(2a) * (-b + sign(b) sqrt( b^2 - 4 ac) )
 *  @return calc_quadroot2: 1/(2a) * (-b - sign(b) sqrt( b^2 - 4 ac) )
 *
 * @{
 */
DLLEXP   double calc_quadroot1( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error );														   	/**< first quadratic root*/
DLLEXP  my_bool calc_quadroot1_init( UDF_INIT *initid, UDF_ARGS *args, char *message );																   	/**< first quadratic root init*/
DLLEXP     void calc_quadroot1_deinit( UDF_INIT *initid );																							   	/**< first quadratic root init*/
DLLEXP   double calc_quadroot2( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error );														   	/**< second quadratic root*/
DLLEXP  my_bool calc_quadroot2_init( UDF_INIT *initid, UDF_ARGS *args, char *message );																   	/**< second quadratic root init*/
DLLEXP     void calc_quadroot2_deinit( UDF_INIT *initid );																							   	/**< second quadratic root init*/
/** @} // end of Mysql_UDF_calc_quadroot_ */

/** @defgroup Mysql_UDF_calc_count_ calc_count_<cmp>
 *  @brief MySQL UDF functions for counting the number of parameters which staisfy a condition
 *  
 *  @see #calc_count_null for counting the number of null argumentse
 *  
 *  @return calc_count_null: number of arguments which are NULL (no conversion of parameters specified)
 *
 * @{
 */
DLLEXP longlong calc_count_null(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);												   		/**< count number of null*/
DLLEXP  my_bool calc_count_null_init( UDF_INIT *initid, UDF_ARGS *args, char *message );															   	/**< calc_count_null init*/
DLLEXP     void calc_count_null_deinit( UDF_INIT *initid );																						   	/**< calc_count_null deinit*/
/** @} // end of Mysql_UDF_calc_count_ */


/** @defgroup Mysql_UDF_decimal calc_dtoa 
 *  @brief MySQL UDF function for printing smallest decimal representation
 *
 *   This is a wrapper to the double_conversion library from 
 *   Florain Loitsch (https://github.com/google/double-conversion).
 *   calc_dtoa print the smallest decimal string representation
 *   which yields the same binary double as input.
 *  
 *  @param a (REAL, required):  quadratic coefficient
 *  @param b (REAL, required):  linear coefiicient
 *  @param c (REAL, required):  constant 
 *  @return calc_quadroot1: 1/(2a) * (-b + sign(b) sqrt( b^2 - 4 ac) )
 *  @return calc_quadroot2: 1/(2a) * (-b - sign(b) sqrt( b^2 - 4 ac) )
 *
 * @{
 */

DLLEXP  char  *calc_dtoa(UDF_INIT *initid, UDF_ARGS *args, char *result,  unsigned long *res_length, char *is_null, char *error);					   	/**< returns the shortest decimal presentation as string*/
DLLEXP my_bool calc_dtoa_init( UDF_INIT *initid, UDF_ARGS *args, char *message );																	   	/**< calc_dtoa init*/
DLLEXP    void calc_dtoa_deinit( UDF_INIT *initid );																									/**< calc_dtoa deinit*/

DLLEXP longlong calc_digits_fractional(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error);													   	/**< returns the number of significant didigts ni the fractional part of a numbre*/
DLLEXP  my_bool calc_digits_fractional_init( UDF_INIT *initid, UDF_ARGS *args, char *message );															   	/**< calc_digits_frac init*/
DLLEXP     void calc_digits_fractional_deinit( UDF_INIT *initid );																								/**< calc_digits_frac deinit*/	


/** @} // end of Mysql_UDF_calc_count_ */






#ifdef	__cplusplus
}
#endif
//#endif /* HAVE_DLOPEN */
