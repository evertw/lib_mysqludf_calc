/** @file calc_cnt.cpp
 *  @brief Implementation of MySQL UDF calc_count_null returning the number of null arguments
 *   
 *  @author Evert Wipplinger
 *  @version 0.0.1
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "lib_mysqludf_calc.h"

/** Initialization of MysQL UDF calc_count_null
 *
 * @note handles constant arguments
 */
my_bool calc_count_null_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	initid->maybe_null=0;
	bool isconst = 1;
	for(size_t i=0; i<args->arg_count;i++) 
	{
		if( args->args[i]==NULL) {			isconst=0;		}
	}
	if( isconst ) 	{		initid->const_item=1;		return 0;
	}

	isconst = 1;
	for(size_t i=0; i<args->arg_count;i++) 
	{
		if( args->maybe_null[i]==1) {			isconst=0;		}
	}
	if( isconst==1 ) {		initid->const_item=1;	}
	return 0;
}

/** Virtual Deitialization of MysQL UDF  calc_count_null*/
void calc_count_null_deinit( UDF_INIT *initid) {};

/** Implementation of MuSQL UDF calc_count_null
 *
 *  @return the number of NULL arguments supplied to the UDF via pointer args
 */
longlong  calc_count_null(UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error)
{

	longlong N=0;
	for(size_t i=0; i<args->arg_count;i++) 
	{
		if( args->args[i]==NULL) {			N++;		}
	}
	return N;
}