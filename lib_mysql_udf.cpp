/** @file lib_mysqludf_calc.cpp
 *  @brief Base file for lib_mysqludf_calc lbrary
 *
 *  Contains the Implementation of lib_mysqludf_calc_info
 * 
 *  @author Evert Wipplinger
 *  @version 0.0.3
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lib_mysqludf_calc.h"

/** Initalization lib_mysqludf_calc_info */
 my_bool lib_mysqludf_calc_info_init(	UDF_INIT *initid,	UDF_ARGS *args,	char *message)
{
	my_bool status;
	if(args->arg_count!=0)
	{
		strcpy(			message		,	"No arguments allowed (udf: lib_mysqludf_calc_info)"		);
		status = 1;
	} else {
		status = 0;
	}
	return status;
}

/**Implementation of lib_mysqludf_calc_info returning LIBERVERSION defined in lib_mysqludf_calc.h */
char* lib_mysqludf_calc_info(	UDF_INIT *initid,	UDF_ARGS *args,	char* result,	unsigned long* length,	char *is_null,	char *error) 
{
	*length = strlen(LIBVERSION);
	return LIBVERSION; 
};

/** virtual deinitialization of lib_mysqludf_calc_info*/
void lib_mysqludf_calc_info_deinit(	UDF_INIT *initid) {} ;