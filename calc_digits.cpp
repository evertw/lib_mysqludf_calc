/** @file calc_digits.cpp
 *  @brief Implementation of the decimal digital calculation UDF MySQL UDF
 * 
 *  @author Evert Wipplinger
 *  @version 0.0.3
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "lib_mysqludf_calc.h"
#include "bestratio.h"
#include "double-conversion.h"
#include "calc_dtoa.h"


/* **************************** Generic ************************************** */

/** Generic First Step of nitialization of #calc_digits_fractional*/
my_bool calc_digits_init(  UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	initid->maybe_null=0;
	bool isconst = 1;
	for(size_t i=0; i<args->arg_count;i++) 
	{
		if( args->args[i]==NULL) {			isconst=0;		}
	}
	if( isconst ) {		initid->const_item=1;		return 0;	}
	isconst = 1;
	for(size_t i=0; i<args->arg_count;i++) 
	{
		if( args->maybe_null[i]==1) {			isconst=0;		}
	}
	if( isconst==1 ) {		initid->const_item=1;	}
	switch (args->arg_count) 
	{
		case 2: args->arg_type[1]=INT_RESULT;
		case 1: args->arg_type[0]=REAL_RESULT; 
		break;
		default : 
			strcpy(message,"calc_digits_init(double,maxfrac_digits) requires 1-2 arguments");			
			return 1;
	}

	return 0;
}

/* **************************** Integer digits ************************************** */

/** Initialisation of calc_digits_integer forces argument to double*/
my_bool calc_digits_integer_init(  UDF_INIT *initid, UDF_ARGS *args, char *message ) {
	if(args->arg_count!=1) 
	{
		strcpy(message,"calc_digits_integer(double) requires 1 arguments");			
		return 1;
	}
	args->arg_type[0]=REAL_RESULT;
}

/** Calculates integer digits using precomputed if statemens in the ratapprox library via ratx_int_digits
 *  @note does not include minus/+ sign
 */
longlong calc_digits_integer( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error ) {
	if(args->args[0]==NULL) {	*is_null=1; return 0;	}
	const double number =  *((double *)args->args[0]);
	const longlong dig = ratx_int_digits(abs(number));
	return dig;
}

/** virtual Deinitialisation of #calc_digits_integer*/
void calc_digits_integer_deinit( UDF_INIT *initid ) {};

/* **************************** fractional digits ************************************** */

/** Initialisation of #calc_digits_fractional
 *  
 * This function first calls the generic initialisation #calc_digits_init
 * and then allocates a #calc_digits_obj object for the calculation of the
 * digits.
 */

my_bool calc_digits_fractional_init(  UDF_INIT *initid, UDF_ARGS *args, char *message )  
{
	const int check = calc_digits_init(initid,args,message);
	if(check) { return 1; }
	try 
	{
		initid->ptr = (char *)new calc_digits_obj;
	} catch(...) { initid->ptr=NULL;	}
	calc_digits_obj * cd = (calc_digits_obj *)initid->ptr;
	return 0;
}

/** Impelmentation of MySQL UDF calc_digits_fractional */
longlong calc_digits_fractional( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error ) {
	if(args->args[0]==NULL) {	*is_null=1; return 0;	}
	longlong requested_digits = 17;
	if(args->arg_count>=2) 
	{
		if(args->args[1]==NULL) {	*is_null=1; return 0;	}
		requested_digits = *((longlong *)args->args[1]);
	}
	const double number =  *((double *)args->args[0]);
	calc_digits_obj * cd = (calc_digits_obj *)initid->ptr;
	//too many digits for );
	const longlong fracdigits = cd->fractional_digits(number,(int)requested_digits);
	DEBUG_PRINTF("\nnumber=%f,\nfp=%f\n,ip=%f\ndig=%d\n",number,fp,ip,(int)fracdigits);
	return fracdigits > 0 ? fracdigits : 0;
}

/** Deinitialisation of calc_digits_integer*/
void calc_digits_fractional_deinit( UDF_INIT *initid ) {
	calc_digits_obj * cd = (calc_digits_obj *)initid->ptr;
	delete cd;
};