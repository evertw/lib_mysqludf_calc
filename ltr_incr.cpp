/** @file  ltr_firstarg.cpp
 *  @Implementation of the incr_comp_ MysSQL UDFs
 *  @author Evert Wipplinger
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "myarg_cmp.h"
#include "lib_mysqludf_calc.h"


/* ********************************** Generic *****************************************/

/** Generic Initialization for incr_comp
 *  
 *  Check for even numbe of arguments and  real or integer input
 *  @todo Handling of const parameters
*/
my_bool incr_comp_init( UDF_INIT *initid, UDF_ARGS *args, char *message )
{

	if (args->arg_count %2 == 1 || args->arg_count==0 ) 
	{
        strcpy(message,"inc_comp_init(nr1a,nr1b,nr2a,nr2b,...): requires even number of arguments checks in pairs e.g. nr1a op nr1b, then nr2a op nr2b");
        return 1;
    }
	for(size_t i=0; i<  args->arg_count; i++) 	
	{
		if(args->arg_type[i]!=INT_RESULT) {  args->arg_type[i] = REAL_RESULT; }
	}
	//initid->decimals= 14;			 //7 decimal places returned (13 before including sign is standard)
	initid->maybe_null=0;

	initid->const_item=1;
	for(size_t i=0; i<  args->arg_count; i++) 	
	{
		if(args->args[i]==NULL) {
			initid->const_item=0; 
			break;
		}
	}
	return 0;
};

/* ********************************** ltr_firstarg_gton *****************************************/

/** Initialization of ltr_incr_gt calls incr_comp_init()
 *  @see ltr_incr_gt_deinit()
 *  @see ltr_incr_gt()
 */
my_bool ltr_incr_gt_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) {
	return incr_comp_init( initid, args, message );
}

/** Virtual deinitialization of ltr_firstarg_gton
 *  @see ltr_firstarg_gton_init()
 *  @see ltr_firstarg_gton()
 */
void ltr_incr_gt_deinit( UDF_INIT *initid) {} ;

/** Implementation of ltr_firstarg_gton MysQL UDF
 *  
 *  @see my_argcmp()
 *  @see ltr_firstarg_gton_init()
 *  @see ltr_firstarg_gton()
 */
longlong ltr_incr_gt(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *is_null
,	char *error
){
	for(size_t i=1; i<args->arg_count; i+=2 ) 
	{
		if(args->args[i-1]==NULL || args->args[i]==NULL) { *is_null=1; return 0; }
		if( my_argcmp(args->args[i-1],args->args[i],args->arg_type[i],args->arg_type[i-1],error) > 0 ) 
		{
			return i; 
		}
	}
	return 0;
}

/* ********************************** ltr_incr_lt *****************************************/

/** Initialization of ltr_incr_lt calls incr_comp_init()
 *  @see ltr_incr_lt_deinit()
 *  @see ltr_incr_lt()
 */
my_bool ltr_incr_lt_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) {
	return incr_comp_init( initid, args, message );
}

/** Virtual deinitialization of ltr_firstarg_lton
 *  @see ltr_firstarg_lton_init()
 *  @see ltr_firstarg_lton()
 */
void ltr_incr_lt_deinit( UDF_INIT *initid) {} ;

/** Implementation of ltr_firstarg_lton MysQL UDF
 *  
 *  @see my_argcmp()
 *  @see ltr_firstarg_lton_init()
 *  @see ltr_firstarg_lton()
 */
longlong ltr_incr_lt(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *is_null
,	char *error
){
	for(size_t i=1; i<args->arg_count; i+=2 ) 
	{
		if(args->args[i-1]==NULL || args->args[i]==NULL) { *is_null=1; return 0; }
		if( my_argcmp(args->args[i-1],args->args[i],args->arg_type[i],args->arg_type[i-1],error) < 0 ) 
		{
			return i; 
		}
	}
	return 0;
}

/* ********************************** ltr_incr_ne *****************************************/

/** Initialization of ltr_incr_ne calls incr_comp_init()
 *  @see ltr_incr_ne_deinit()
 *  @see ltr_incr_ne()
 */
my_bool ltr_incr_ne_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) {
	return incr_comp_init( initid, args, message );
}
void ltr_incr_ne_deinit( UDF_INIT *initid) {} ;

longlong ltr_incr_ne(
	UDF_INIT *initid
,	UDF_ARGS *args
,	char *is_null
,	char *error
){
	for(size_t i=1; i<args->arg_count; i+=2 ) 
	{
		if(args->args[i-1]==NULL || args->args[i]==NULL) { *is_null=1; return 0; }
		if( my_argcmp(args->args[i-1],args->args[i],args->arg_type[i],args->arg_type[i-1],error) != 0 ) 
		{
			return i; 
		}
	}
	return 0;
}