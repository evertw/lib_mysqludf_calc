/** @file  myarg_cmp.h
 *  @brief by value comparison of pointers conditional on Item_result - type in MySQL
 *  @author Evert Wipplinger
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#ifndef MYARG_COMP_H
#define MYARG_COMP_H


#include <m_ctype.h>
#include "mysql.h"

/** compares arguments of a mysql stucture 
 *  @todo specialised version
 *  make specialised versions of this such that the switch statement becomes unneccessary
 *  however note the semantics and special rules in function/template specialisation vs. overloads.
 *
 *  @param a (REAL or INT) pointer to left hand side of comarison
 *  @param b (REAL* Or INT*) pointer to right hand side of comparison
 *  @param ta (MySQL Result Type) INT_RESULT or REAL_RESULT for a
 *  @param tb (MySQL Result Type) INT_RESULT or REAL_RESULT for b
 *  @param error char point to error indicator
 *  @return +1 if a>b
 *  @return  0 if a==b
 *  @return -1 if a<b
 *  @return set *error=1 if there was no comparison possible
 *
 *  @todo Since the type is not changing this function can be made into a template without
 *  a switch statement.
 * see #ltr_firstarg_
 */
inline static int my_argcmp(const void * a,const void *b, const int  ta , const int tb,char * error) {
	switch( ta ) {
			case INT_RESULT : 
				switch(tb ) {
					case INT_RESULT : 
						if ( *((longlong *)a) > *((longlong *)b) ) { return 1; } 
						if ( *((longlong *)a) < *((longlong *)b) ) { return -1; } 
					break;
					case REAL_RESULT:
						if ( (double)*((longlong *)a) > *((double *)b) ) { return 1; } 
						if ( (double)*((longlong *)a) < *((double *)b) ) { return -1; } 	
					break;
					default : *error=1;
				}
			break;
			case REAL_RESULT:
				switch( tb ) {
					case INT_RESULT : 
						if ( *((double *)a) > (double)*((longlong *)b) ) { return 1; } 
						if ( *((double *)a) < (double)*((longlong *)b) ) { return -1; } 
					break;
					case REAL_RESULT:
						if ( *((double *)a) > *((double *)b) ) { return 1; } 
						if ( *((double *)a) < *((double *)b) ) { return -1; } 
					break;
					default: *error=1;
				}
			break;
			default : * error=1;
	}
	return 0;
}

template <typename T> int signum(T val) {
    return (T(0) < val) - (val < T(0));
}

template<Item_result S1,Item_result S2>  struct my_fixed_argcmmp 
{
	int operator () (const void * a,const void *b,char * error) 
	{
		return my_argcmp( a,b, S1, S2, error);
	}
};


template<> struct my_fixed_argcmmp<REAL_RESULT,REAL_RESULT> 
{
	int operator () (const void * a,const void *b,char * error )
	{
		return signum(  *((double *)a) - (double)*((longlong *)b)  );
	};

};

template<> struct my_fixed_argcmmp<INT_RESULT,INT_RESULT> 
{
	int operator () (const void * a,const void *b,char * error )
	{
		return signum(  *((longlong *)a) - (double)*((longlong *)b)  );
	};

};

template<> struct my_fixed_argcmmp<REAL_RESULT,INT_RESULT> 
{
	int operator () (const void * a,const void *b,char * error )
	{
		return signum(  *((double *)a) - (double)*((longlong *)b)  );
	};

};

template<> struct my_fixed_argcmmp<INT_RESULT,REAL_RESULT> 
{
	int operator () (const void * a,const void *b,char * error )
	{
		return signum(  *((longlong *)a) - (double)*((double *)b)  );
	};

};

	



#endif /*MYARG_COMP_H*/