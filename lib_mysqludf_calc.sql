USE test;

DROP FUNCTION IF EXISTS lib_mysqludf_calc_info;
DROP FUNCTION IF EXISTS calc_quadroot1;
DROP FUNCTION IF EXISTS calc_quadroot2;

DROP FUNCTION IF EXISTS ltr_incr_gt;
DROP FUNCTION IF EXISTS ltr_incr_lt;


DROP FUNCTION IF EXISTS ltr_firstarg_gton;
DROP FUNCTION IF EXISTS ltr_firstarg_lton;
DROP FUNCTION IF EXISTS ltr_firstarg_ge;
DROP FUNCTION IF EXISTS ltr_firstarg_le;

DROP FUNCTION IF EXISTS calc_count_null;

CREATE FUNCTION lib_mysqludf_calc_info RETURNS STRING SONAME 'lib_mysqludf_calc.dll';


/* a+bx+cx^2 = 0 */

/*calc_quadroot1 select high (real) solution to quadratic eqation*/
CREATE FUNCTION calc_quadroot1 RETURNS real SONAME 'lib_mysqludf_calc.dll';
/*calc_quadroot1 select high (real) solution to quadratic eqation*/
CREATE FUNCTION calc_quadroot2 RETURNS real SONAME 'lib_mysqludf_calc.dll';



/* incrementally compares paris from left to right*/
/* the left most pair is compared ( a1,b1, .... if cannot be computed because of missing
the next pair  ( ... ,a2,b2,.... is computed until a match (lt:striclty lower than, gt stricly greater than) can be computed*/
/*this is useful for comparing dates year, month, date if month or days are unknwon*/
CREATE FUNCTION ltr_incr_gt RETURNS int SONAME 'lib_mysqludf_calc.dll';
CREATE FUNCTION ltr_incr_lt RETURNS int SONAME 'lib_mysqludf_calc.dll';

SELECT ltr_incr_gt(2001,2002,10,10,01,01);
SELECT ltr_incr_gt(2003,2002,10,10,01,01);
SELECT ltr_incr_gt(2001,2002,NULL,10,01,01);
SELECT ltr_incr_gt(2003,2002,10,NULL,01,01);
SELECT ltr_incr_gt(2002,2002,NULL,10,01,01);
SELECT ltr_incr_gt(2002,2002,10,NULL,01,01);

SELECT ltr_incr_lt(2001,2002,10,10,01,01);
SELECT ltr_incr_lt(2003,2002,10,10,01,01);
SELECT ltr_incr_lt(2001,2002,NULL,10,01,01);
SELECT ltr_incr_lt(2003,2002,10,NULL,01,01);
SELECT ltr_incr_lt(2002,2002,NULL,10,01,01);
SELECT ltr_incr_lt(2002,2002,10,NULL,01,01);

CREATE FUNCTION ltr_firstarg_gton RETURNS int SONAME 'lib_mysqludf_calc.dll';
CREATE FUNCTION ltr_firstarg_lton RETURNS int SONAME 'lib_mysqludf_calc.dll';
CREATE FUNCTION ltr_firstarg_ge RETURNS int SONAME 'lib_mysqludf_calc.dll';
CREATE FUNCTION ltr_firstarg_le RETURNS int SONAME 'lib_mysqludf_calc.dll';
CREATE FUNCTION calc_count_null RETURNS int SONAME 'lib_mysqludf_calc.dll';

SELECT calc_count_null(2001,0,10,10,01,01);
SELECT calc_count_null(2003,2002,10,10,01,01);
SELECT calc_count_null(2001,2002,NULL,10,01,01);
SELECT calc_count_null(2003,2002,10,NULL,01,01);
SELECT calc_count_null(2002,2002,NULL,10,01,01);
SELECT calc_count_null(2002,NULL,10,NULL,01,01);

CREATE FUNCTION calc_dtoa RETURNS string SONAME 'lib_mysqludf_calc.dll';

SELECT 0.299999999999999988897,calc_dtoa(0.299999999999999988897);
SELECT CAST( 0.299999999999999988897 AS DECIMAL(2,1) ) as c1;
SELECT CAST( 0.299999999999999988897 AS DECIMAL(2,1) ) *1.0 as c2;
SELECT CAST(calc_dtoa(0.299999999999999988897) AS DECIMAL(2,1) )  as c3;
SELECT CAST(calc_dtoa(0.299999999999999988897) AS DECIMAL(2,1) )*1.0 as c4;

SELECT CONVERT( 0.299999999999999988897 , DECIMAL(2,1) ) as c1;
SELECT CONVERT( 0.299999999999999988897 , DECIMAL(2,1) ) *1.0 as c2;
SELECT CONVERT(calc_dtoa(0.299999999999999988897) , DECIMAL(2,1) ) ) as c3;
SELECT CONVERT(calc_dtoa(0.299999999999999988897) , DECIMAL(2,1) )*1.0 as c4;


drop function calc_bestratio_for_round;
create function calc_bestratio_for_round returns real soname 'lib_mysqludf_calc.dll';

  drop function calc_digits_fractional;
create function calc_digits_fractional returns int soname 'lib_mysqludf_calc.dll';

