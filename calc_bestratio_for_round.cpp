/** @file calc_bestratio_for_round.cpp
 *  @brief Implementation of the calc_bestratio_for_round MySQL UDF
 * 
 *  @author Evert Wipplinger
 *  @version 0.0.3
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lib_mysqludf_calc.h"
#include "bestratio.h"
#include "calc_dtoa.h"


/* sign function template
 *
 * @remark readable version for bithacks see http://graphics.stanford.edu/~seander/bithacks.html
 */
template <typename T> int sgn(T val) 
{
	return (T(0) < val ) - (val<T(0));
};
/** @fn double round(double d)
 *  a simple implementation of rounding to the nearest integer
*/
inline long double round(long double d) 
{
	return ( d < 0.0 ? ceil(d -0.5) : floor(d+0.5));
}
/** @fn double roundcmp(double a, double b, int factor) {
  * checks if two double are equal up to a rounding after pultiplying by a factor 
  * @warning overflow is possible hence modf should be used outside
  * @return 1 if variables a and b are equal when rounded after multiplyng by a factor factor
  */
 double roundcmp(const double a, const double b, int factor) 
 {
	 //double ia,ib;
	 //const double fa = modf(a,&ia);
	 //const double fb = modf(b,&ib);
	 //if(ia!=ib) { return 0;}
	 return round(a*factor) == round(b*factor);
}
/** ratx_ratio find_best_rat_for_round (const double,const ulong,const longlong)
 *  @brief The best (smallest denominator) rational approximation which rounds to the target ratio
 *  
 *  This function calculates the best rational apprxoimation to a rational number in
 *  the sense that it has the smallest denominator for which the following holds:
 *  The decimal representation of the rational approximation can be recovered
 *  when rounding at a specified number of digits \a round_at_digits.
 *
 *  @note The denominator is increasted by powers of 10 so the returned denominator can be
 *  the next power of ten larger than the parameter \a max_denom supplied.
 *  @warning The returned ratio is only guaranteed to satisfy the requirements if \a round_at_digits
 *  is smaller or equal to \a max_denom. Otherwose see the note.
 *  @attention The function relies on the external library \b bestratio
 *  
 *  @param target_ratio number which should be approximated
 *  @param max_denom the highest denominator which should be considered for the decimal part
 *  @param round_at_digits the number of digits at which rounding must be equal
 *  @return ratx_ratio, a structure from the bestratio library where numerator = ratx_ratio.num, and the denominator = ratx_ratio.denom
 *
 *  @see ratx_ratio
 *  @see bestratio
 *  @remark ratx_find_best_rat_cont is faster for expected cases
 *  @remark There is a small overhead using mof since this is also performed in calc_bestratio_for_round
 *  but it will be easier to refactor this
 *  
  */
 double find_best_rat_for_round(const double target_ratio,const unsigned int max_denom,const int round_at_digits) 
 {
    ulong sum_niter = 0;
	double integer_part;
	double decimal_part = modf(target_ratio,&integer_part);
	const double rounder = pow(10.0,round_at_digits);
	const double decimal_part_rounded = round(fabs(decimal_part) * rounder);
	//1.) checkthe denom=1 case
	if( round(decimal_part) == decimal_part ) 
	{
		return target_ratio;
	}
	
	
	//2.) find the best rational apprxoimation 
	//    a lower appproximation could be found
	//ratx_ratio rat = ratx_find_best_rat_cont(target_ratio,max_denom);
	ratx_ratio rat = ratx_find_best_rat_cont(decimal_part,max_denom);

	DEBUG_PRINTF("initial %llu/%llu=%f (niter=%llu)\n",(unsigned long long)(rat.nom),(unsigned long long)(rat.denom),(double)(rat.nom)/rat.denom
		,(unsigned long long)rat.niter);

	//3.) perform a binary search
	double rat_approx = ratx_get_ratio(&rat);
	if (round( rat_approx * rounder) != decimal_part_rounded ) 
	{
		//not even the best rational approximation matches the rounding

	DEBUG_PRINTF("ratfind infeasible ratio %llu/%llu=%f (%f) max_denom=%llu\n"
				,(unsigned long long)(rat.nom),(unsigned long long)(rat.denom),(double)(rat.nom)/rat.denom,target_ratio
				,(unsigned long long) max_denom
				);

		//return rat;
	}
	ulonglong low = 2;
	ulonglong hig = rat.denom-1;
	ulonglong mid;
	ratx_ratio rat_at_mid;
	while (low <= hig) {
		mid = low + (hig-low)/2;
		rat_at_mid = ratx_find_best_rat_cont(decimal_part,mid);
		rat_approx = ratx_get_ratio(&rat_at_mid);
		if( round( rat_approx * rounder ) == decimal_part ) 
		{
			// there is a lower feasible denominator in the lower half

			DEBUG_PRINTF("ratfind found new ratio %llu/%llu=%f better than %llu/%llu=%f (%f)\n"
				,(unsigned long long)(rat_at_mid.nom),(unsigned long long)(rat_at_mid.denom),(double)(rat_at_mid.nom)/rat_at_mid.denom
				,(unsigned long long)(rat.nom),(unsigned long long)(rat.denom),(double)(rat.nom)/rat_at_mid.denom
				,target_ratio
				);

			rat = rat_at_mid;
			hig = rat_at_mid.denom -1;
		} else {
			// there could be a better approximator in the upper half
			low = mid + 1;
		}

		DEBUG_PRINTF("ratfind low=%d mid=%d hig=%d atmid=%f (%f)\n"
				,(int)low,(int)mid,(int)hig,rat_approx,(double)(rat.nom)/rat.denom
				);

	} //end while

	DEBUG_PRINTF("final %d/%d=%f (niter=%d)\n",(int)(rat.nom),(int)(rat.denom),(double)(rat.nom)/rat.denom,(int)rat.niter);
	return integer_part+rat_approx;
}

/** calc_bestratio_for_round_S
 *  the storage structure for calc_bestratio_for_round
 *  @see calc_bestratio_for_round
    @see calc_digits_obj
*/
typedef struct _calc_bestratio_for_round_S 
{
	calc_digits_obj digits; /**< used to calculate the number of significant digits in the target_ratio*/
	/** a wrapper to find_best_rat_for_round
	 *  @see find_best_rat_for_round
	 */
} calc_bestratio_for_round_S;

/** calc_bestratio_for_round_obj @see _calc_bestratio_for_round_S */
typedef calc_bestratio_for_round_S calc_bestratio_for_round_obj;

/** calc_bestratio_for_round_deinit
  * Deinitialisation @see calc_bestratio_for_round_init @see calc_bestratio_for_round
  */
void calc_bestratio_for_round_deinit( UDF_INIT *initid ) 
{
	calc_bestratio_for_round_obj * d = (calc_bestratio_for_round_obj *) initid->ptr;
	if( d!=NULL) {delete d; }

};

/** calc_bestratio_for_round_init
 *  Initialisation @see calc_bestratio_for_round_deinit @see calc_bestratio_for_round
 *  @return Returns 0 on success;
 */
my_bool calc_bestratio_for_round_init(  UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	initid->maybe_null=0;
	bool isconst = 1;
	for(size_t i=0; i<args->arg_count;i++) {
		if( args->args[i]==NULL) {			isconst=0;		}
	}
	for(size_t i=0; i<args->arg_count;i++) {
		if( args->maybe_null[i]==1) {			isconst=0;		}
	}
	if( isconst==1 ) {		initid->const_item=1;	}
	switch ( args->arg_count ) {
		case 3:			args->arg_type[2] = INT_RESULT;
		case 2:			args->arg_type[1] = INT_RESULT;
		case 1:			args->arg_type[0] = REAL_RESULT;
		break;
		default :
			strcpy(message,"calc_bestratio_for_round_init(target_ratio,min_denom,max_denom) requires 2-3 arguments");
			initid->ptr = NULL;
			return 1;
	}

	//if( args->arg_count <=2 ) {
		if( !( initid->ptr = (char*) new calc_bestratio_for_round_obj ) ) {
			strcpy(message,"calc_bestratio_for_round_init( could not create dtoa for automatic rounding at significant digits");
			return 1;
		}
	//} else {
	//	initid->ptr = NULL;
	//}
	initid->decimals=17;
	return 0;
}
/* calc_bestratio_for_round see header */
double calc_bestratio_for_round( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error ) 
{
	
	int point,length;
	for(size_t i=0;i<args->arg_count;i++) 
	{
		if( args->args[i] == NULL) {			*is_null=1; 			return 0.0;		}
	}
	
	const double target_ratio = *( (double*)(args->args[0]));
	calc_bestratio_for_round_obj * d = (calc_bestratio_for_round_obj *) initid->ptr;
	//1.fractional part
	double ipart;
	double fpart = modf(target_ratio,&ipart);
	//2.digit representation
	const int fractional_digits = d->digits.fractional_digits(fpart, 17  );
	point = d->digits.point; 
	length = d->digits.length; 
	if( fpart==0 || point >= length ) { return target_ratio ;  } //integer
	//3. configuration
	longlong round_at_digits = 17, round_at_least_at_digits = 0;
	switch (args->arg_count) 
	{
		case 3: round_at_digits = *( (longlong *)(args->args[2]) ); 
				round_at_least_at_digits =  *( (longlong *)(args->args[1]) );
			break;
		case 2: round_at_least_at_digits =  *( (longlong *)(args->args[1]) );
		case 1:	round_at_digits = fractional_digits;
#ifdef DEBUG
			printf("calc_bestratio_for_round: round_at_digits=%d\n",(int)round_at_digits);
#endif
	}
	round_at_digits = (round_at_digits<round_at_least_at_digits ? round_at_least_at_digits : round_at_digits);

	//return if theres a cutoff
	if(round_at_digits<=0) { return round(target_ratio); }
	//4. discard leading zeros
	const int first_fractional_digit = ( point<0 ? -point : 0 );
	const double upart = pow( 10 , log10(fpart)   + first_fractional_digit );
	const int new_round_at_digits = (int)( round_at_digits>17 ? 17 : round_at_digits) - first_fractional_digit;
	const int max_denom = (int) pow(10.0, new_round_at_digits) ;
		
	
	double rat_approx = find_best_rat_for_round( upart
		                                    ,(int) abs(max_denom )
											,(int)new_round_at_digits
											);
	rat_approx = pow( 10 , log10(rat_approx)-first_fractional_digit );
	return rat_approx + ipart;
}


