/** @file calc_dtoa.h
 *  @brief Header for the object passed in in calc_dtoa
 *
 *  The file defines a struct typedef'ed to dtoa which implements
 *	the double_conserion library from Florain Loitsch (https://github.com/google/double-conversion)
 *   
 *  @author Evert Wipplinger
 *  @version 0.0.1
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "double-conversion.h"
#include <math.h>
/** dtoa_S 
 *  @brief This is an object handing double conversion
 *  @note must be initialized, deinitialized after construction, before deconstruction
 *  @see dtoa_S::init @see dtoa_S::deinit()
 *  @note requires the external library double_conversion from Florian Loitsch.
 *  @see dtoa
*/    
typedef struct _dtoa_S {
	const size_t buffer_len;																										    /**< buffer length of #buffer*/
	char *buffer;																														/**< char buffer of size #buffer_len*/
	double_conversion::DoubleToStringConverter *dtoa;																					/**< dtoa routine from double_conversion*/
	double_conversion::StringBuilder *str;																								/**< formatter froom double_vonversion*/
	_dtoa_S() : buffer_len(double_conversion::DoubleToStringConverter::kBase10MaximalLength+1 ) { dtoa=NULL; str=NULL; buffer =NULL; };
	/** @fn bool init(int decimal_in_shortest_low,int decimal_in_shortest_high) 
	  * initializes the dtoa structure with parameters for switching to exponential represetnation
	  * @warning It yields incorrect output for number of digits 
	  * if the parameter supplied yield a conversion to exponential representation.
	  * @see dtoa_S::init()
	  * @param decimal_in_shortest_low exponent at which to switch to exponential representation (low)
	  * @param decimal_in_shortest_high at which to switch to exponential representation (high)
	  * @return return 0 on success
	  * @return 1 on failure
	  */
	bool init(int decimal_in_shortest_low,int decimal_in_shortest_high)
	{
		buffer = new char[buffer_len];
		if(buffer==NULL) { return 1;}
		str = new double_conversion::StringBuilder(buffer,buffer_len);
		if(str == NULL) { delete[] buffer; return 1; }
		dtoa = new double_conversion::DoubleToStringConverter(
			 double_conversion::DoubleToStringConverter::Flags::UNIQUE_ZERO | double_conversion::DoubleToStringConverter::Flags::EMIT_POSITIVE_EXPONENT_SIGN
			,"Inf" //const char* infinity_symbol,
			,"NaN" //const char* nan_symbol,
			,'e'  //char exponent_character,
			,decimal_in_shortest_low //int decimal_in_shortest_low,
			,decimal_in_shortest_high //int decimal_in_shortest_high,
			,0 //int max_leading_padding_zeroes_in_precision_mode,
			,0 //int max_trailing_padding_zeroes_in_precision_mode
		);
		if(dtoa == NULL) {	 delete dtoa; delete[] buffer;		}
		return 0;
	};
	/** initializes the dtoa structure with  init(INT_MIN,INT_MAX)
	  * @see dtoa_S::init()
	  */
	bool init() { return init(INT_MIN,INT_MAX); };
	/** deinitializes the dtoa structure with  init(INT_MIN,INT_MAX)
	  * @see dtoa_S::init()
	  */
	void deinit() {
		if(dtoa!=NULL) { delete dtoa; dtoa = NULL; }
		if(str!=NULL) {delete str;	str=NULL;}
		if(buffer!=NULL) { delete[] buffer; buffer = NULL; }
	};
} dtoa_S;

/** dtoa @see dtoa_S */
typedef dtoa_S dtoa;

/** @struct _calc_digits_S
 *  @brief This is an object for calculating the number of significant digits in the
 *  decimal representation of a number.
 *  @note requires the external library double_conversion from Florian Loitsch.
 */   
typedef struct _calc_digits_S {
	double_conversion::DoubleToStringConverter * dtoa;									/**< dtoa routine from double_conversion*/
	char * buffer;																		/**< character buffer of size #buffer_len*/		
	const int buffer_len;															/**< size of #buffer*/
	bool sign;																			/**< sign of the supplied number*/
	int length;																			/**< length of the shortest representation*/
	int point;																			/**< offset of the decimal point: e.g. 0.033: point=1; 100.00: point=2 */
	/** constructor: initializes #dtoa and #buffer*/
	_calc_digits_S() : buffer_len(128) {
		buffer = new char[buffer_len];
		dtoa = new double_conversion::DoubleToStringConverter(
			 double_conversion::DoubleToStringConverter::Flags::NO_FLAGS
			,0 //const char* infinity_symbol,
			,0 //const char* nan_symbol,
			,'e'  //char exponent_character,
			,-61 //int decimal_in_shortest_low,
			,61 //int decimal_in_shortest_high,
			,0 //int max_leading_padding_zeroes_in_precision_mode,
			,0 //int max_trailing_padding_zeroes_in_precision_mode
		);
	}
	/** Returns the number of significant digits without trainling zeros
	@param a real number
	@param requested_digits the highest number of requested digits (integer+fractional part)
	@return the shortest representation
	*/
	int significant_digits(const double a,const int requested_digits) 
	{
		const int rd = ( requested_digits > 61 ? 61 : requested_digits );
		dtoa->DoubleToAscii(a
			,double_conversion::DoubleToStringConverter::DtoaMode::SHORTEST
			,rd,buffer,buffer_len,&sign,&length,&point
		);
		return length;
	}
	/** Returns the number of significant digits without tainling zeros of the fractional part of a number.
	@param a real number
	@param requested_digits the highest number of requested digits (fractional part only)
	@return the shortest representation
	*/
	int fractional_digits(const double a,const int requested_digits) 
	{
		double ipart;
		const double fpart = modf(a,&ipart);
		const int rd = ( requested_digits > 61 ? 61 : requested_digits );
		dtoa->DoubleToAscii(fpart
			,double_conversion::DoubleToStringConverter::DtoaMode::SHORTEST
			,rd
			,buffer,buffer_len,&sign,&length,&point
		);
		return length-point;
	}
	/** deconstructor deletes #dtoa and #buffer */
	~_calc_digits_S() {
		delete dtoa;
		delete buffer;
	}
} calc_digits_S;

/** @typedef calc_digits_obj
 *  This is the typedef for calc_digits_S.
 *  @see calc_digits_S
 */
typedef calc_digits_S calc_digits_obj;