/** @file  ltr_firstarg.cpp
 *  @Implementation of the ltr_firstarg_ MysSQL UDFs
 *  @author Evert Wipplinger
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "myarg_cmp.h"
#include "lib_mysqludf_calc.h"


/* ********************************** Generic *****************************************/

/** Generic Initialization for firstgroup_comp UDFs
 *  
 *  Check for real or integer input and at least 2 arguments supplied to the UDF
 *  @todo Handling of const parameters
 */
my_bool firstgroup_comp_init( UDF_INIT *initid, UDF_ARGS *args, char *message )
{
	if (args->arg_count < 2 ) 
	{
        strcpy(message,"arg_comp_init(nr1a,nr1b,nr2a,nr2b,...): requires at least another argument to compare to the first");
        return 1;
    }
	for(size_t i=0; i<  args->arg_count; i++) 	
	{
		if( args->arg_type[i]!=INT_RESULT) { args->arg_type[i]=REAL_RESULT; }
	}
	//initid->decimals= 14;			 //7 decimal places returned (13 before including sign is standard)
	initid->maybe_null=1;
	return 0;
};

/* ********************************** ltr_firstarg_gton *****************************************/

/** Initialization of ltr_firstarg_gton calls firstgroup_comp_init()
 *  @see ltr_firstarg_gton_deinit()
 *  @see ltr_firstarg_gton()
 */
my_bool ltr_firstarg_gton_init( UDF_INIT *initid, UDF_ARGS *args, char *message )
{
	return firstgroup_comp_init( initid, args, message );
}
/** Virtual deinitialization of ltr_firstarg_gton
 *  @see ltr_firstarg_gton_init()
 *  @see ltr_firstarg_gton()
 */
void ltr_firstarg_gton_deinit( UDF_INIT *initid) {} ;

/** Implementation of ltr_firstarg_gton MysQL UDF
 *  
 *  @see my_argcmp()
 *  @see ltr_firstarg_gton_init()
 *  @see ltr_firstarg_gton()
 */
longlong ltr_firstarg_gton(	UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error)
{
	if( args->args[0] == NULL ) {		*is_null =1;		return 0;	}
	int il=args->arg_count;
	for(size_t i=1; i<args->arg_count; i++ ) 
	{
		if(args->args[i]==NULL) { return i; } /*or null gt [ on ] */
		if( my_argcmp(args->args[0],args->args[i],args->arg_type[0],args->arg_type[i],error) > 0 ) 
		{
			return i; 
		}
	}
	return 0;
}
/* ********************************** ltr_firstarg_lton *****************************************/


/** Initialization of ltr_firstarg_gton calls firstgroup_comp_init()
 *  @see ltr_firstarg_lton_deinit()
 *  @see ltr_firstarg_lton()
 */
my_bool ltr_firstarg_lton_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	return firstgroup_comp_init( initid, args, message );
}

/** Virtual deinitialization of ltr_firstarg_gton
 *  @see ltr_firstarg_lton_init()
 *  @see ltr_firstarg_lton()
 */
void ltr_firstarg_lton_deinit( UDF_INIT *initid) {} ;

/** Implementation of ltr_firstarg_lton MysQL UDF
 *  
 *  @see my_argcmp()
 *  @see ltr_firstarg_lton_init()
 *  @see ltr_firstarg_lton()
 */
longlong ltr_firstarg_lton(	UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error)
{
	if( args->args[0] == NULL ) {		*is_null =1;		return 0;	}
	int il=args->arg_count;
	for(size_t i=1; i<args->arg_count; i++ ) 
	{
		if(args->args[i]==NULL) { return i; } /*or null gt [ on ] */
		if( my_argcmp(args->args[0],args->args[i],args->arg_type[0],args->arg_type[i],error) < 0 ) {
			return i; 
		}
	}
	return il;
}

/* ********************************** ltr_firstarg_ge *****************************************/

/** Initialization of ltr_firstarg_ge calls firstgroup_comp_init()
 *  @see ltr_firstarg_ge_deinit()
 *  @see ltr_firstarg_ge()
 */
my_bool ltr_firstarg_ge_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	return firstgroup_comp_init( initid, args, message );
}

/** Virtual deinitialization of ltr_firstarg_ge
 *  @see ltr_firstarg_ge_init()
 *  @see ltr_firstarg_ge()
 */
void ltr_firstarg_ge_deinit( UDF_INIT *initid) {} ;

/** Implementation of ltr_firstarg_ge MysQL UDF
 *  
 *  @see my_argcmp()
 *  @see ltr_firstarg_ge_init()
 *  @see ltr_firstarg_ge()
 */
longlong ltr_firstarg_ge(	UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error)
{
	if( args->args[0] == NULL ) {		*is_null =1;		return 0;	}
	int il=args->arg_count;
	size_t countnull=0;
	for(size_t i=1; i<args->arg_count; i++ ) 
	{
		if(args->args[i]==NULL) { countnull++; } /*or null gt [ on ] */
		if( my_argcmp(args->args[0],args->args[i],args->arg_type[0],args->arg_type[i],error) >= 0 ) 
		{
			return i; 
		}
	}
	if( countnull==(il-1) ) { *is_null=1;}
	return 0;
}

/* ********************************** ltr_firstarg_le *****************************************/

/** Initialization of ltr_firstarg_le calls firstgroup_comp_init()
 *  @see ltr_firstarg_le_deinit()
 *  @see ltr_firstarg_le()
 */
my_bool ltr_firstarg_le_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	return firstgroup_comp_init( initid, args, message );
}

/** Virtual deinitialization of ltr_firstarg_le
 *  @see ltr_firstarg_le_init()
 *  @see ltr_firstarg_le()
 */
void ltr_firstarg_le_deinit( UDF_INIT *initid) {} ;

/** Implementation of ltr_firstarg_le MysQL UDF
 *  
 *  @see my_argcmp()
 *  @see ltr_firstarg_le_init()
 *  @see ltr_firstarg_le()
 */
longlong ltr_firstarg_le(	UDF_INIT *initid,	UDF_ARGS *args,	char *is_null,	char *error)
{
	if( args->args[0] == NULL ) {		*is_null =1;		return 0;	}
	int il=args->arg_count;
	size_t countnull=0;
	for(size_t i=1; i<args->arg_count; i++ ) 
	{
		if(args->args[i]==NULL) { countnull++; } /*or null gt [ on ] */
		if( my_argcmp(args->args[0],args->args[i],args->arg_type[0],args->arg_type[i],error) <= 0 ) 
		{
			return i; 
		}
	}
	if( countnull==(il-1) ) { *is_null=1;}
	return il;
}