/** @file cal_dtoa.cpp
 *  @brief Implementation of the calc_dtoa MySQL UDF
 * 
 *  @author Evert Wipplinger
 *  @version 0.0.3
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lib_mysqludf_calc.h"
#include <string>
#include "calc_dtoa.h"

/** Initialization routine for calc_dtoa UDF
 *  
 *  The Initialization handles input checks and allocats a dtoa object
 *  @see calc_dtoa_deinit()
 *  @see calc_dtoa()
 */
my_bool calc_dtoa_init( UDF_INIT *initid, UDF_ARGS *args, char *message )
{
	if (args->arg_count < 1)
    {
        strcpy(message,"calc_dtoa(double[,low_exp][,hig_exp]): requires at least 1 arguments");
        return 1;
    }
	args->arg_type[0] = REAL_RESULT; //v the double input
	if (args->arg_count > 1 ) 
	{
		args->arg_type[1] = INT_RESULT; //decimal_in_shortest_low
		if( args->args[1] == NULL ) 
		{
			strcpy(message,"calc_dtoa(double[,low_exp][,hig_exp]): low_exp must be constant");
			return 1;
		}

	}
	if (args->arg_count > 2 ) 
	{
		args->arg_type[2] = INT_RESULT; //decimal_in_shortest_low
		if( args->args[2] == NULL ) 
		{
			strcpy(message,"calc_dtoa(double[,low_exp][,hig_exp]): hig_exp must be constant");
			return 1;
		}
	}

	initid->ptr = NULL;
	if( !(initid->ptr= (char *) new dtoa)) 
	{
		strcpy(message,"calc_dtoa(double[,low_exp][,hig_exp]): out of memory 1");
			return 1;
	}
	dtoa *d = (dtoa*) initid->ptr;
	const bool check = d->init(args->arg_count > 1 ? (int)*((longlong *)args->args[1]) : INT_MIN
		                      ,args->arg_count > 2 ? (int)*((longlong *)args->args[2]) : INT_MAX
	);
	if( check!=0 || d->dtoa == NULL || d->str == NULL || d->buffer==NULL) 
	{
		strcpy(message,"calc_dtoa(double[,low_exp][,hig_exp]): out of memory 2");
		delete d;
		return 1;
	}
	initid->max_length=double_conversion::DoubleToStringConverter::kBase10MaximalLength+4;
	initid->maybe_null=1;
	return 0;
};

/** Deinitialization for clac_dtoa MysQL UDF functions
 *  deletes dtoa object
 */
void calc_dtoa_deinit( UDF_INIT *initid ) 
{
	dtoa *d = (dtoa*) initid->ptr;
	d->deinit();
	delete d;
	return;
}

/** Main Function of MySQL UDF calc_dota
 * which conerts a double to its shortest string representation
 * as a wrapper to the double_conversion library of Florian Loitsch
 * which is allocated in a struct by calc_dtoa_init 
 */
char *calc_dtoa(UDF_INIT *initid, UDF_ARGS *args, char *result,  unsigned long *res_length, char *is_null, char *error) 
{
	dtoa *d = (dtoa*) initid->ptr;
	if( args->args[0]==NULL) 
	{
		*is_null=1;
		return 0;
	}
	d->str->Reset();
	d->dtoa->ToShortest( *((double *)args->args[0]) , d->str );
	*res_length = d->str->position();
	return d->buffer;


}