/** @file cal_quadroot.cpp
 *  @brief Implementation of the calc_quadrooot MySQL UDF
 * 
 *  @author Evert Wipplinger
 *  @version 0.0.1
 *  @copyright GNU Pulic License version 3 or later
*/
/*
This file is part of lib_mysqludf_calc.

    lib_mysqludf_calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    lib_mysqludf_calc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with lib_mysqludf_calc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "lib_mysqludf_calc.h"

/* @def sign
 * simple sign
 * @return -1, 0 ,+1 for <0, ==0, >0
 */
#define sign(x) ( (x > 0) ? 1 : ((x < 0) ? -1 : 0) )

/* ********************************** Generic *****************************************/

/*Generic initialization of the two calc_quadrooot MySQL UDF */
static my_bool calc_quadroot_init( UDF_INIT *initid, UDF_ARGS *args, char *message )
{
	if (args->arg_count < 3) {
        strcpy(message,"calc_quadroot(a,b,c): requires at least 3 arguments");
        return 1;
    }
	for(size_t i=0; i<  args->arg_count; i++) 	
	{
		args->arg_type[i] = REAL_RESULT;
	}
	initid->decimals= 14;			 //7 decimal places returned (13 before including sign is standard)
	initid->maybe_null=1;
	return 0;
};

/* ********************************** Init *****************************************/

/*Generic initialization of the two calc_quadrooot1 MySQL UDF 
 *
 * calls calc_quadroot_init()
 */
my_bool calc_quadroot1_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	return calc_quadroot_init(initid, args,message );
}

/*Generic initialization of the two calc_quadrooot2 MySQL UDF 
 *
 * calls calc_quadroot_init()
 */
my_bool calc_quadroot2_init( UDF_INIT *initid, UDF_ARGS *args, char *message ) 
{
	return calc_quadroot_init(initid, args,message );
}

/* ********************************** Deinit *****************************************/

/** Virtual Deinitialization calc_quadroot1*/
void calc_quadroot1_deinit( UDF_INIT *initid ) {};
/** Virtual Deinitialization calc_quadroot2*/
void calc_quadroot2_deinit( UDF_INIT *initid ) {};

/** calculates the first quadratic root
 *   -0.5 * (b + sign(b)* sqrt(b*b-4*a*c) ) / a;
 */

/* ********************************** calc_quadroot *****************************************/

/** Implementation of calc_quadroot1 (first root of quadratic polynom)
  *
  * @return -0.5 * (b + sign(b)* sqrt(b*b-4*a*c) )
  */
double calc_quadroot1( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error ) 
{
	for(size_t i=0; i<  3; i++) 
	{
		if( args->args[i] == NULL) 
		{
			*is_null=1;
			return 0;
		}
	}

	const double a = *(double*)(args->args[0]);
	const double b = *(double*)(args->args[1]);
	const double c = *(double*)(args->args[2]);

	if(b==0.0) 
	{
		return + sqrt(-c/a);
	}
	const double q = -0.5 * (b + sign(b)* sqrt(b*b-4*a*c) );
	if(q!=q) { *is_null=1; }
	return q/a;
}

/** Implementation of calc_quadroot1 (second root of quadratic polynom)
  *
  * @return -0.5 * (b - sign(b)* sqrt(b*b-4*a*c) )
  */
double calc_quadroot2( UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error ) 
{
	for(size_t i=0; i<  3; i++) 
	{
		if( args->args[i] == NULL) 
		{
			*is_null=1;
			return 0;
		}
	}
	const double a = *(double*)(args->args[0]);
	const double b = *(double*)(args->args[1]);
	const double c = *(double*)(args->args[2]);

	if(b==0.0) 
	{
		return - sqrt(-c/a);
	}
	const double q = -0.5 * (b - sign(b)* sqrt(b*b-4*a*c) );
	if(q!=q) { *is_null=1; }
	return c/a;
}